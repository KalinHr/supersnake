#include "snakeheadelement.h"

#include <QKeyEvent>
#include <QDebug>
#include <QBrush>

SnakeHeadElement::SnakeHeadElement(int aElementSize)
{
    _elementSize = aElementSize;

    _up.append(QPointF(_elementSize/2, 0));
    _up.append(QPointF(0, _elementSize));
    _up.append(QPointF(_elementSize, _elementSize));
    _up.append(QPointF(_elementSize/2, 0));

    _down.append(QPointF(_elementSize/2, _elementSize));
    _down.append(QPointF(0, 0));
    _down.append(QPointF(_elementSize, 0));
    _down.append(QPointF(_elementSize/2, _elementSize));

    _left.append(QPointF(0, _elementSize/2));
    _left.append(QPointF(_elementSize, 0));
    _left.append(QPointF(_elementSize, _elementSize));
    _left.append(QPointF(0, _elementSize/2));

    _right.append(QPointF(_elementSize, _elementSize/2));
    _right.append(QPointF(0, 0));
    _right.append(QPointF(0, _elementSize));
    _right.append(QPointF(_elementSize, _elementSize/2));

    this->setPolygon(_right);

    this->setBrush(QBrush(QColor(34, 139, 34)));

    setFlag(QGraphicsItem::ItemIsFocusable);
    setFocus();
}

void SnakeHeadElement::keyPressed(QKeyEvent *event)
{
    switch (event->key())
    {
        case Qt::Key_Left:
        {
            if (direction() != Direction::Right)
            {
                _newDirection = Direction::Left;
            }
        }
            break;
        case Qt::Key_Right:
        {
            if (direction() != Direction::Left)
            {
                _newDirection = Direction::Right;
            }
        }
            break;
        case Qt::Key_Up:
        {
            if (direction() != Direction::Down)
            {
                _newDirection = Direction::Up;
            }
        }
            break;
        case Qt::Key_Down:
        {
            if (direction() != Direction::Up)
            {
                _newDirection = Direction::Down;
            }
        }
            break;
        default:
            break;
    }
}

void SnakeHeadElement::move()
{
    switch (direction())
    {
        case Direction::Left:
        {
            setPos(x() - 1, y());
            break;
        }
        case Direction::Right:
        {
            setPos(x() + 1, y());
            break;
        }
        case Direction::Up:
        {
            setPos(x(), y() - 1);
            break;
        }
        case Direction::Down:
        {
            setPos(x(), y() + 1);
            break;
        }
        default:
        {
            break;
        }
    }
}

int SnakeHeadElement::elementSize()
{
    return _elementSize;
}

void SnakeHeadElement::setNewDirection()
{
        setDirection(_newDirection);

        switch (direction())
        {
            case Direction::Left:
            {
                this->setPolygon(_left);
            }
                break;
            case Direction::Right:
            {
                this->setPolygon(_right);
            }
                break;
            case Direction::Up:
            {
                this->setPolygon(_up);
            }
                break;
            case Direction::Down:
            {
                this->setPolygon(_down);
            }
                break;
            default:
                break;
        }
}
