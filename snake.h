#ifndef SNAKE_H
#define SNAKE_H

#include "snakeheadelement.h"
#include "snakebodyelement.h"

#include <QWidget>
#include <Qlist>

class Snake : public QWidget
{
    Q_OBJECT
public:
    Snake(int aElementSize);
    SnakeHeadElement* head();
    QList<SnakeBodyElement *>* body();
    void addElement();
    void move();

private:
    SnakeHeadElement* _head;
    QList<SnakeBodyElement *>* _body;
    int _elementSize;
};

#endif // SNAKE_H
