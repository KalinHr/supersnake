#include "snakegraphicsview.h"

#include <QResizeEvent>
#include <QKeyEvent>
#include <ctime>
#include <QBrush>
#include <QGraphicsPixmapItem>
#include <QImage>
#include <QPixmap>
#include <QPropertyAnimation>
#include <QParallelAnimationGroup>
#include <QDebug>

SnakeGraphicsView::SnakeGraphicsView(QWidget *parent)
    : QGraphicsView(parent)
    , _scene(new QGraphicsScene)
    , _field(new QList<QList<FieldElement*> *>)
    , _timer(new QTimer)
    //, _slider(new QSlider(Qt::Horizontal))
{
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    setElementSize(50);

    _scene->setSceneRect(0, 0, width() - (width() % elementSize()), height() - (height() % elementSize()));

    this->setScene(_scene);
}

void SnakeGraphicsView::setField()
{
    _field->clear();
    _scene->clear();

    for(int i = 0; i < width() / elementSize(); i++)
    {
        QList<FieldElement*>* list = new QList<FieldElement*>;
        for (int j = 0; j < height() / elementSize(); j++)
        {
            FieldElement* fieldElement = new FieldElement(elementSize());

            fieldElement->setPos(i * elementSize(), j * elementSize());
            list->append(fieldElement);
        }
        _field->append(list);
    }

    for(int i = 0; i < _field->size(); i++)
    {
        for(int j = 0; j < _field->at(i)->size(); j++)
        {
            _scene->addItem(_field->at(i)->at(j));
        }
    }
}

void SnakeGraphicsView::setNewGame()
{
    this->setFocus();

    this->setMinimumWidth(width());
    this->setMaximumWidth(width());
    this->setMinimumHeight(height());
    this->setMaximumHeight(height());

    _snake = new Snake(elementSize());

    setSnake();

    const QPixmap pm( ":/images/Mouse.png" );
    _food = new Food(pm.scaled(elementSize(), elementSize(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation), elementSize());
    _food->setTransformationMode(Qt::SmoothTransformation);

    setFood();

    connect(_timer, SIGNAL(timeout()), this, SLOT(nextFrame()));
    _timer->start(5);
}

void SnakeGraphicsView::setSnake()
{
    _snake->head()->setPos(0, 0);

    for (int i = 0; i < 3; i++)
    {
        _snake->addElement();
    }

    for (int i = _snake->body()->size() - 1; i >= 0; i--)
    {
        _scene->addItem(_snake->body()->at(i));
    }
    _scene->addItem(_snake->head());
}

void SnakeGraphicsView::setFood()
{
    _scene->removeItem(_food);
    _food->stopTimerAndAnimation();
    srand(time(NULL));
    int randX = (rand() % (width() / elementSize())) * elementSize();
    int randY = (rand() % (height() / elementSize())) * elementSize();
    _food->setScale(1.0);
    _food->setPos(randX, randY);
    _food->setElementX(randX);
    _food->setElementY(randY);
    _scene->addItem(_food);
    _food->startTimer();
}

void SnakeGraphicsView::setElementSize(int aSize)
{
    _elementSize = aSize;
}

int SnakeGraphicsView::elementSize()
{
    return _elementSize;
}

void SnakeGraphicsView::resizeEvent(QResizeEvent *event)
{
    setField();
}

void SnakeGraphicsView::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Left
            || event->key() == Qt::Key_Right
            || event->key() == Qt::Key_Up
            || event->key() == Qt::Key_Down)
    {
        _snake->head()->keyPressed(event);
    }
}

void SnakeGraphicsView::nextFrame()
{
    if (_snake->head()->x() == _food->elementX() && _snake->head()->y() == _food->elementY())
    {
        _snake->addElement();
        _scene->removeItem(_snake->body()->at(_snake->body()->size() - 2));
        _scene->addItem(_snake->body()->last());
        _scene->addItem(_snake->body()->at(_snake->body()->size() - 2));
        setFood();
    }

    switch (_snake->head()->direction())
    {
    case SnakeElement::Direction::Left:
    {
        if (_snake->head()->x() < 0)
        {
            _timer->stop();
        }
    }
        break;
    case SnakeElement::Direction::Right:
    {
        if (_snake->head()->x() > (width() / elementSize()) *  elementSize() - elementSize())
        {
            _timer->stop();
        }
    }
        break;
    case SnakeElement::Direction::Up:
    {
        if (_snake->head()->y() < 0)
        {
            _timer->stop();
        }
    }
        break;
    case SnakeElement::Direction::Down:
    {
        if (_snake->head()->y() > (height() / elementSize()) * elementSize() - elementSize())
        {
            _timer->stop();
        }
    }
        break;
    default:
        break;
    }

    for (int i = 0; i <= _snake->body()->size() - 1; i++)
    {
        switch (_snake->head()->direction())
        {
        case SnakeElement::Direction::Left:
        {
            if (_snake->head()->x() == _snake->body()->at(i)->x() + elementSize()
                    && _snake->head()->y() == _snake->body()->at(i)->y())
            {
                _snake->body()->at(i)->setBrush(QBrush(QColor(70, 70, 70)));
                _timer->stop();
            }
        }
            break;
        case SnakeElement::Direction::Right:
        {
            if (_snake->head()->x() == _snake->body()->at(i)->x() - elementSize()
                    && _snake->head()->y() == _snake->body()->at(i)->y())
            {
                _snake->body()->at(i)->setBrush(QBrush(QColor(70, 70, 70)));
                _timer->stop();
            }
        }
            break;
        case SnakeElement::Direction::Up:
        {
            if (_snake->head()->x() == _snake->body()->at(i)->x()
                    && _snake->head()->y() == _snake->body()->at(i)->y() + elementSize())
            {
                _snake->body()->at(i)->setBrush(QBrush(QColor(70, 70, 70)));
                _timer->stop();
            }
        }
            break;
        case SnakeElement::Direction::Down:
        {
            if (_snake->head()->x() == _snake->body()->at(i)->x()
                    && _snake->head()->y() == _snake->body()->at(i)->y() - elementSize())
            {
                _snake->body()->at(i)->setBrush(QBrush(QColor(70, 70, 70)));
                _timer->stop();
            }
        }
            break;
        default:
            break;
        }
    }

    _snake->move();
}
