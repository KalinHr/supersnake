#ifndef SNAKEHEADELEMENT_H
#define SNAKEHEADELEMENT_H

#include "snakeelement.h"

#include <QObject>
#include <QGraphicsPolygonItem>

class SnakeHeadElement : public QObject, public QGraphicsPolygonItem, public SnakeElement
{
    Q_OBJECT
public:
    SnakeHeadElement(int aElementSize);
    void keyPressed(QKeyEvent* event);
    void move();
    int elementSize();
    void setNewDirection();

private:
    int _elementSize;
    Direction _newDirection;

    QPolygonF _up;
    QPolygonF _down;
    QPolygonF _left;
    QPolygonF _right;
};

#endif // SNAKEHEADELEMENT_H
