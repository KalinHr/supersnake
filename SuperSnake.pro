QT += core gui
QT += widgets

SOURCES += \
    main.cpp \
    snakeelement.cpp \
    snakeheadelement.cpp \
    snake.cpp \
    snakebodyelement.cpp \
    fieldelement.cpp \
    food.cpp \
    mainwindow.cpp \
    snakegraphicsview.cpp

HEADERS += \
    snakeelement.h \
    snakeheadelement.h \
    snake.h \
    snakebodyelement.h \
    fieldelement.h \
    food.h \
    mainwindow.h \
    snakegraphicsview.h

RESOURCES += \
    images.qrc
