#include "fieldelement.h"

#include <QBrush>

FieldElement::FieldElement(int aElementSize)
{
    this->setRect(QRectF(0, 0, aElementSize, aElementSize));
    this->setBrush(QBrush(QColor(222, 184, 135)));
}
