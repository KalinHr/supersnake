#ifndef SNAKEGRAPHICSVIEW_H
#define SNAKEGRAPHICSVIEW_H

#include "snake.h"
#include "fieldelement.h"
#include "food.h"

#include <QGraphicsView>
#include <QList>
#include <QTimer>
#include <QSlider>
#include <QGraphicsProxyWidget>

class SnakeGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    SnakeGraphicsView(QWidget *parent = Q_NULLPTR);
    void setElementSize(int aSize);
    void setNewGame();
    void setField();

private:

    void setSnake();
    void setFood();
    int  elementSize();
    void resizeEvent(QResizeEvent* event);
    void keyPressEvent(QKeyEvent* event);

public slots:
    void nextFrame();

private:
    int _elementSize;

    QGraphicsScene* _scene;
    Snake* _snake;
    Food* _food;
    QList<QList<FieldElement*>*>* _field;
    QTimer* _timer;
    QSlider* _slider;
};

#endif // SNAKEGRAPHICSVIEW_H
