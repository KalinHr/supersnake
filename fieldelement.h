#ifndef FIELDELEMENT_H
#define FIELDELEMENT_H

#include <QGraphicsRectItem>

class FieldElement : public QGraphicsRectItem
{
public:
    FieldElement(int aElementSize);
};

#endif // FIELDELEMENT_H
