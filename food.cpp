#include "food.h"

#include <QPropertyAnimation>
#include <QPixmap>
#include <QParallelAnimationGroup>

Food::Food(const QPixmap &pixmap, int aElementSize)
    : QGraphicsPixmapItem(pixmap)
    , _parallelAnimationGroup(new QParallelAnimationGroup)
{
    _elementSize = aElementSize;

    _timer = new QTimer(this);
    connect(_timer, SIGNAL(timeout()), this, SLOT(puls()));
}

void Food::setElementX(int aX)
{
    _elementX = aX;
}

qreal Food::elementX()
{
    return _elementX;
}

void Food::setElementY(int aY)
{
    _elementY = aY;
}

qreal Food::elementY()
{
    return _elementY;
}

void Food::startTimer()
{
//    _timer->start(10000);
    _timer->start(5000);
}

void Food::stopTimerAndAnimation()
{
    _timer->stop();
    _parallelAnimationGroup->stop();
}

void Food::puls()
{
    QPropertyAnimation *scaleAnimation = new QPropertyAnimation(this, "scale");
    scaleAnimation->setDuration(1000);
    scaleAnimation->setKeyValueAt(0.0, 1.0);
    scaleAnimation->setKeyValueAt(0.5, 0.8);
    scaleAnimation->setKeyValueAt(1.0, 1.0);
//        scaleAnimation->setEasingCurve(QEasingCurve::OutInBounce);

    QPropertyAnimation *posAnimation = new QPropertyAnimation(this, "pos");
    posAnimation->setDuration(1000);
    posAnimation->setKeyValueAt(0.0, QPointF(this->x(), this->y()));
    posAnimation->setKeyValueAt(0.5, QPointF(this->x() + 0.1 * elementSize(), this->y() + 0.1 * elementSize()));
    posAnimation->setKeyValueAt(1.0, QPointF(this->x(), this->y()));
//        posAnimation->setEasingCurve(QEasingCurve::OutInBounce);

    _parallelAnimationGroup->addAnimation(scaleAnimation);
    _parallelAnimationGroup->addAnimation(posAnimation);
    _parallelAnimationGroup->setLoopCount(3);
    _parallelAnimationGroup->start();
}

int Food::elementSize()
{
    return _elementSize;
}

