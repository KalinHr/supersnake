#include "snakebodyelement.h"

#include <QBrush>

SnakeBodyElement::SnakeBodyElement(int aElementSize)
{
    this->setRect(QRectF(0, 0, aElementSize, aElementSize));
    this->setBrush(QBrush(QColor(34, 139, 34)));
}

void SnakeBodyElement::move()
{
    switch (direction())
    {
        case Direction::Left:
        {
            setPos(x() - 1, y());
            break;
        }
        case Direction::Right:
        {
            setPos(x() + 1, y());
            break;
        }
        case Direction::Up:
        {
            setPos(x(), y() - 1);
            break;
        }
        case Direction::Down:
        {
            setPos(x(), y() + 1);
            break;
        }
        default:
        {
            break;
        }
    }
}
