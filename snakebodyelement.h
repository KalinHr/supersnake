#ifndef SNAKEBODYELEMENT_H
#define SNAKEBODYELEMENT_H

#include "snakeelement.h"

#include <QObject>
#include <QGraphicsEllipseItem>

class SnakeBodyElement : public QObject, public QGraphicsEllipseItem, public SnakeElement
{
    Q_OBJECT
public:
    SnakeBodyElement(int aElementSize);
    void move();
};

#endif // SNAKEBODYELEMENT_H
