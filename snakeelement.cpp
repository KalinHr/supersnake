#include "snakeelement.h"

#include <QGraphicsScene>
#include <QKeyEvent>

void SnakeElement::setDirection(SnakeElement::Direction aDirection)
{
    _direction = aDirection;
}

SnakeElement::Direction SnakeElement::direction()
{
    return _direction;
}
