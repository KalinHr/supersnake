#include "snake.h"

Snake::Snake(int aElementSize)
    : _head(new SnakeHeadElement(aElementSize))
    , _body(new QList<SnakeBodyElement *>)
    , _elementSize(aElementSize)
{
}

SnakeHeadElement *Snake::head()
{
    return _head;
}

QList<SnakeBodyElement *> *Snake::body()
{
    return _body;
}

void Snake::move()
{
    _head->move();
    for (int i = 0; i < _body->size(); i++)
    {
        _body->at(i)->move();
    }

    int x = _head->x();
    int y = _head->y();
    if (x % _head->elementSize() == 0 && y % _head->elementSize() == 0)
    {
        if (!_body->isEmpty())
        {
            for (int i = _body->size() - 1; i > 0; i--)
            {
                _body->at(i)->setDirection(_body->at(i - 1)->direction());
            }

            _body->first()->setDirection(_head->direction());
        }

        _head->setNewDirection();
    }

}

void Snake::addElement()
{
    SnakeBodyElement* newElement = new SnakeBodyElement(_elementSize);
    if (_body->isEmpty())
    {
        newElement->setPos(_head->x(), _head->y());
    }
    else
    {
        newElement->setPos(_body->last()->x(), _body->last()->y());
    }

    _body->append(newElement);
}
