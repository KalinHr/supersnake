#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "snakegraphicsview.h"

#include <QMainWindow>
#include <QSlider>
#include <QObject>

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = Q_NULLPTR);

private:
    void keyPressEvent(QKeyEvent* event);

public slots:
    void updateFieldSize(int aSize);

private:
    SnakeGraphicsView* _snakeGraphicsView;
    QSlider* _slider;
};

#endif // MAINWINDOW_H
