#include "mainwindow.h"

#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , _snakeGraphicsView(new SnakeGraphicsView)
    , _slider(new QSlider(Qt::Horizontal))
{
    setFocus();
    _slider->setMinimum(40);
    _slider->setMaximum(100);
    _snakeGraphicsView->setElementSize(_slider->minimum());

    connect(_slider, SIGNAL(valueChanged(int)), this, SLOT(updateFieldSize(int)));

    QVBoxLayout* const mainLayout = new QVBoxLayout();
    mainLayout->addWidget(_snakeGraphicsView);
    mainLayout->addWidget(_slider);

    QWidget *window = new QWidget();
    window->setLayout(mainLayout);

    setCentralWidget(window);
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Return)
    {
        this->setMinimumWidth(width());
        this->setMaximumWidth(width());
        this->setMinimumHeight(height());
        this->setMaximumHeight(height());

        _snakeGraphicsView->setFocus();
        _snakeGraphicsView->setNewGame();
        _slider->setEnabled(false);
    }
}

void MainWindow::updateFieldSize(int aSize)
{
    _snakeGraphicsView->setElementSize(aSize);
    _snakeGraphicsView->setField();
}
