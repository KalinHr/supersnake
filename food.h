#ifndef FOOD_H
#define FOOD_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QTimer>
#include <QParallelAnimationGroup>

class Food: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    Q_PROPERTY(qreal scale READ scale WRITE setScale)
    Q_PROPERTY(QPointF pos READ pos WRITE setPos)
public:
    Food(const QPixmap &pixmap, int aElementSize);
    void setElementX(int aX);
    qreal elementX();
    void setElementY(int aY);
    qreal elementY();

    void startTimer();
    void stopTimerAndAnimation();

public slots:
    void puls();

private:
    int elementSize();

private:
    int _elementSize;
    qreal _elementX;
    qreal _elementY;
    QTimer* _timer;
    QParallelAnimationGroup* _parallelAnimationGroup;
};

#endif // FOOD_H
