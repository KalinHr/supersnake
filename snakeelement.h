#ifndef SNAKEELEMENT_H
#define SNAKEELEMENT_H

class SnakeElement
{
public:
    enum Direction
    {
        Left,
        Right,
        Up,
        Down
    };

public:
    void setDirection(Direction aDirection);
    Direction direction();

private:
    Direction _direction;
};

#endif // SNAKEELEMENT_H
